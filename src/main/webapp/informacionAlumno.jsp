<%-- 
    Document   : index.jsp
    Created on : 05-jun-2021, 0:27:58
    Author     : voice
--%>

<%@page import="cl.evaluacion02.entitys.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% 
    Alumnos alumno = (Alumnos)request.getAttribute("alumno");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles.css" type="text/css" rel="stylesheet">
        <title>Información alumno</title>
    </head>
    <body>
        <div id="div-fullpage">

            <nav id="nav">
                <a href="RegistrarController" class="nav-link">Registrar nuevo alumno</a>
                <a href="ConsultarController" class="nav-link">Consultar alumno</a>
                <a href="EliminarController" class="nav-link">Eliminar alumno</a>
            </nav>

            <div class="category-separation">
                <div class="category-container">
                    <p class="title" id="portfolio"> Datos del alumno </p>
                </div>

                <hr>

                <!-- Formulario para registrar alumno -->
                <form name="form" action="ModificarController" method="POST">
                    <div class="form">

                        <div class="field"> 
                            <h5>Rut:</h5>
                            <input step="any"  id="rut" name="rut"  value="<%= alumno.getRut() %>">
                        </div>

                        <div class="field"> 
                            <h5>Nombres:</h5>
                            <input step="any" id="nombres" name="nombres" value="<%= alumno.getNombres() %>">
                        </div>

                        <div class="field"> 
                            <h5>Apellidos:</h5>
                            <input step="any"  id="apellidos" name="apellidos"  value="<%= alumno.getApellidos() %>">
                        </div>
                        
                        <div class="field"> 
                            <h5>Teléfono:</h5>
                            <input step="any"  id="telefono" name="telefono" value="<%= alumno.getTelefono() %>">
                        </div>
                        
                        <div class="field"> 
                            <h5>Correo:</h5>
                            <input step="any"  id="correo" name="correo" value="<%= alumno.getCorreo() %>">
                        </div>
                        
                        <div class="field"> 
                            <button type="submit" name="accion" value="ingresar" class="submit"> Modificar datos </button>
                        </div>

                    </div>

                </form>


            </div>
        </div>
    </body>
</html>
