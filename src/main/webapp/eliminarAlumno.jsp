<%-- 
    Document   : index.jsp
    Created on : 05-jun-2021, 0:27:58
    Author     : voice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles.css" type="text/css" rel="stylesheet">
        <title>Eliminar alumno</title>
    </head>
    <body>
        <div id="div-fullpage">

            <nav id="nav">
                <a href="RegistrarController" class="nav-link">Registrar nuevo alumno</a>
                <a href="ConsultarController" class="nav-link">Consultar alumno</a>
                <a href="EliminarController" class="nav-link">Eliminar alumno</a>
            </nav>


            <div class="category-separation">
                <div class="category-container">
                    <p class="title" id="portfolio"> Eliminar alumno </p>
                </div>

                <hr>

                <!-- Formulario para registrar alumno -->
                <form name="form" action="EliminarController" method="POST">
                    <div class="form">

                        <div class="field"> 
                            <h5>Rut:</h5>
                            <input step="any"  id="rut" name="rut" placeholder="rut">
                        </div>

                        <div class="field-btn"> 
                            <button type="submit" name="accion" value="ingresar" class="btn-eliminar"> Eliminar </button>
                        </div>

                    </div>
                </form>


            </div>
        </div>
    </body>
</html>
