<%-- 
    Document   : index.jsp
    Created on : 05-jun-2021, 0:27:58
    Author     : voice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles.css" type="text/css" rel="stylesheet">
        <title>Alumnos</title>
    </head>
    <body>
        <div id="div-fullpage">
            
            <nav id="nav">
                <a href="RegistrarController" class="nav-link">Registrar nuevo alumno</a>
                <a href="ConsultarController" class="nav-link">Consultar alumno</a>
                <a href="EliminarController" class="nav-link">Eliminar alumno</a>
            </nav>
            
        </div>
    </body>
</html>
